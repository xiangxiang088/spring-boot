package com.bjsxt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot访问静态资源1
 * 1. SpringBoot 从 classpath/static 的目录（注意：必须要放到static目录下面)
 *
 * 访问3个方法都可以：
 * http://127.0.0.1:8080/index.html
 * http://127.0.0.1:8080/liuyifei.png
 * http://127.0.0.1:8080/
 */
@SpringBootApplication
public class App 
{
    public static void main( String[] args ) {
        SpringApplication.run(App.class,args);
    }
}
