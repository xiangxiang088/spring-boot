package com.qx.controller;

import com.qx.pojo.Users;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * SpringBoot整合jsp
 */
@Controller  //这里不能用RestController,因为这里需要跳转，而不是返回json数据
public class UserContrller {

    /**
     *处理请求，产生数据
     * @return List<Users>
     */
    @GetMapping("/showUser")
    public String showUser(Model model){
        System.out.println("----------------------showUser---------------------------");
        List<Users> list = new ArrayList<>();
        list.add(new Users(1,"中国移动","10086"));
        list.add(new Users(2,"中国联通","10010"));
        list.add(new Users(3,"中国电信","10000"));

        //需要一个model对象
        model.addAttribute("list",list);
        //跳转视图jsp
        return "userList";

    }
}
