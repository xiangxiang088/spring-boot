package com.qx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 1.SpringBoot整合view-thymeleaf
 * 1.1 创建项目
 * 1.2 pom.xml文件新加thymeleaf启动器
 * 1.3 创建目录要求：src/main/resources/templates   templates说明：该目录是安全的，意味着该目录的内容是不允许外界直接访问（通俗来讲就是不能在浏览器指定地址访问，必须通过controller渲染）
 *
 * 2.thymeleaf的基本使用；
 * 2.1 thymeleaf的特点：使用它们指定的标签
 * 2.2 thymeleaf创建html
 * 2.3 thymelwaf使用标签 th:text=""向页面输入一串字符串
 * 2.4 编写启动类
 * 2.5 <meta charset="UTF-8">报错解决方案：
 *    方法1：给结束标签 <meta charset="UTF-8"/>
 *    方法2：在pom.xml 修改thymeleaf的3.0和2.0以上的版本：<thymeleaf-version>3.0.2.RELWASE</thymeleaf-version>  <thymeleaf-layout-dialect.version>2.0.2.RELEASE</thymeleaf-layout-dialect.version>
 *
 * 3.Thymeleaf语法讲解：
 *  3.1变量输出与字符串
 *      3.1.1 tx:text="" 输出字符串
 *      3.1.2 tx:value="" 向input框赋值
 *      3.1.3 th:text="${#strings.isEmpty(key)}" 判断字符串是否为空 如果为空返回true，否则返回false
 *      3.1.4 th:text="${#strings.contains(key,'9')}" 判断一个字符串包涵什么字符，区分大小写
 *      还有很多，请去本项目看index.html看案例
 *
 */
@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {
        SpringApplication.run(App.class,args);
        System.out.println( "Hello World!" );
    }
}
