package com.qx.entity;

/**
 * 实体类
 */
public class Users {
    private int userId;
    private String userName;
    private String phone;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    //有参构造方法
    public Users(int userId, String userName, String phone) {
        super();
        this.userId = userId;
        this.userName = userName;
        this.phone = phone;
    }

    //无参构造方法
    public Users() {
        super();
    }
}
