package com.qx.controller;

import com.qx.entity.Users;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Thymeleaf的入门案例
 *
 */
@Controller
public class DemoController {

    @RequestMapping("/show")
    public String showInfo(Model model){
        model.addAttribute("msg","Thymeleaf 的第一个案例：HelloWorld");
        model.addAttribute("code","");
        model.addAttribute("date",new Date());
        return "index";
    }

    @RequestMapping("/show2")
    public String showInfo2(Model model){
        model.addAttribute("sex",'女');
        model.addAttribute("id",2);
        return "index2";
    }

    @RequestMapping("show3")
    public String showInfo3(Model model){
        List<Users> list = new ArrayList<>();
        list.add(new Users(1,"中国移动","10086"));
        list.add(new Users(2,"中国联通","10010"));
        list.add(new Users(3,"中国电信","10000"));
        model.addAttribute("list",list);
        return "index3";
    }

    @RequestMapping("show4")
    public String showInfo4(Model model){
        Map<String,Object> map = new HashMap<>();
        map.put("user1",new Users(1,"中国移动","10086"));
        map.put("user2",new Users(2,"中国联通","10010"));
        map.put("user3",new Users(3,"中国电信","10000"));
        model.addAttribute("map",map);
        return "index4";
    }

    @RequestMapping("show5")
    public String showInfo4(HttpServletRequest req,Model model){
        req.setAttribute("req","HttpServletRequest");
        req.getSession().setAttribute("sess","HttpSession");
        req.getSession().getServletContext().setAttribute("app","Application");
        return "index5";
    }

}
