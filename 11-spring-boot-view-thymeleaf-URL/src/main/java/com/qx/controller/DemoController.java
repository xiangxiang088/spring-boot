package com.qx.controller;

import org.slf4j.ILoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DemoController {

    @RequestMapping("/{page}")
    public String showInfo(@PathVariable String page,Integer id,String name){
        System.out.println(id);
        System.out.println(name);
        return page;
    }

}
