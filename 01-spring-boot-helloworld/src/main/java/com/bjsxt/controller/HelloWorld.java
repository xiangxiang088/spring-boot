package com.bjsxt.controller;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
@SpringBootApplication
public class HelloWorld {

    @RequestMapping("/hello")
    @ResponseBody
    public Map<String,Object> hello(){
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("code","0000");
        map.put("message","HelloWorld");
        return map;
    }
}
