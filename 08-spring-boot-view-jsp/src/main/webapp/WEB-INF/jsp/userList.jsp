<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <table border="1" align="center" width="60%">
        <tr>
            <th>用户id</th>
            <th>用户名称</th>
            <th>用户电话</th>
        </tr>
        <c:forEach items="${list}" var="user">
            <tr>
                <td>${user.userId}</td>
                <td>${user.userName}</td>
                <td>${user.phone}</td>
            </tr>
        </c:forEach>


    </table>

    <img alt="" src="/images/jingtian.jpg">
</body>
</html>