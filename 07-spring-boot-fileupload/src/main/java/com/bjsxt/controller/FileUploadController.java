package com.bjsxt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

//@Controller
@RestController //表示该类下的方法返回值会自动转换为json格式的转换，不需要再去每个方法添加
public class FileUploadController {

    @RequestMapping("/fileUploadController")
    public Map<String,Object> fileUpload(MultipartFile fileName) throws IOException {
        System.out.println("上传文件名称："+fileName.getOriginalFilename());
        fileName.transferTo(new File("D:\\"+fileName.getOriginalFilename()));
        Map<String,Object> map = new HashMap<String,Object>(2);
        map.put("code","0000");
        map.put("msg","success");
        return map;

    }
}
