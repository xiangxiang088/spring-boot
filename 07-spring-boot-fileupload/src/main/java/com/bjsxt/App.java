package com.bjsxt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * Hello world!
 *
 */
//@Controller
//@RestController //表示该类下的方法返回值会自动做json格式的转换
@SpringBootApplication
public class App {
    public static void main( String[] args ) {
        SpringApplication.run(App.class,args);
        System.out.println( "Hello World!" );
    }
}
