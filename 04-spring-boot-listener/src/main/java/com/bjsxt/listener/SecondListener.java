package com.bjsxt.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * SpringBoot整合listener方式二：通过方法完成 Listener 组件注册
 *<listener>
 *     <listener-class>
 *         com.bjsxt.listener.FirstListener
 *     </listener-class>
 *</listener>
 */
public class SecondListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("-------------------contextDestroyed-----------------------------");
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("------------------SencondListener------------int--------------------------");
    }
}
