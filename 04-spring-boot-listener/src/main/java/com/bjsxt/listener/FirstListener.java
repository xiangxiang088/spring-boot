package com.bjsxt.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.ServletContextAttributeEvent;

/**
 * SpringBoot整合listener方式一：通过注解扫描完成 Listener 组件的注册
 *<listener>
 *     <listener-class>
 *         com.bjsxt.listener.FirstListener
 *     </listener-class>
 *</listener>
 */

@WebListener
public class FirstListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Listener---------init-------");
    }
}
