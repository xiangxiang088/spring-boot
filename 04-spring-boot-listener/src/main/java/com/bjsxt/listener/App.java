package com.bjsxt.listener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * SpringBoot整合listener方式一：通过注解扫描完成 Listener 组件的注册
 */
@SpringBootApplication
@ServletComponentScan
public class App {

    public static void main(String[] args){
        SpringApplication.run(App.class,args);
    }
}
