package com.qx.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 控制层
 * 在eclipse可以，但在idea不可以，有待去发现这个bug(已发现)
 * IDEA配置
 * 当我们修改了Java类后，IDEA默认是不自动编译的，而spring-boot-devtools又是监测classpath下的文件发生变化才会重启应用，所以需要设置IDEA的自动编译：
 * （1）File-Settings-Compiler-Build Project automatically
 * （2）ctrl + shift + alt + /,选择Registry,勾上 Compiler autoMake allow when app running
 *
 * 区别：
 * SpringLoader：SpringLoader 在部署项目时使用的是热部署的方式。（修改后台代码，需要手动去运行才起作用）
 * DevTools：DevTools 在部署项目时使用的是重新部署的方式（修改后台和前端代码，后台会重新部署，编译运行）
 *
 * 拓展：
 * （1） devtools可以实现页面热部署（即页面修改后会立即生效，这个可以直接在application.properties文件中配置spring.thymeleaf.cache=false来实现），
 * 实现类文件热部署（类文件修改后不会立即生效），实现对属性文件的热部署。
 * 即devtools会监听classpath下的文件变动，并且会立即重启应用（发生在保存时机），注意：因为其采用的虚拟机机制，该项重启是很快的
 * （2）配置了后在修改java文件后也就支持了热启动，不过这种方式是属于项目重启（速度比较快的项目重启），会清空session中的值，也就是如果有用户登陆的话，项目重启后需要重新登陆。
 *
 * 默认情况下，/META-INF/maven，/META-INF/resources，/resources，/static，/templates，/public这些文件夹下的文件修改不会使应用重启，但是会重新加载（devtools内嵌了一个LiveReload server，当资源发生改变时，浏览器刷新）。
 */
@Controller
public class UserController {

    @RequestMapping("index")
    public String showIndex(){
        System.out.println("-----------HelleWorld---------9999999----------------");
        return "index";
    }
}
