package com.qx.exception;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Properties;

///**
// * 全局捕捉异常变量信息:方法一【推荐】-这里先注释起来
// * ModelAndView
// */
//@ControllerAdvice
//public class GlobalException {
//
//    /**
//     * java.lang.ArithmeticException（捕捉算数异常）
//     * 该方法是需要返回一个ModelAndView,用于捕捉异常和返回指定的错误页面
//     *
//     * Exception e：捕捉产生异常，注入这个方法中，然后将错误信息返回给前端提示
//     */
//    @ExceptionHandler(value={java.lang.ArithmeticException.class})
//    public ModelAndView arithmeticException(Exception e){
//        ModelAndView mv = new ModelAndView();
//        mv.addObject("error",e.toString());
//        mv.setViewName("errorTwo");
//        return mv;
//    }
//
//    /**
//     * java.lang.NullPointerException（捕捉空指针异常）
//     * 该方法需要返回一个ModelAndView的页面：目的是专门捕捉空指针异常然后返回指定的页面中
//     *
//     * Exception e:捕捉异常，然后将异常注入该方法中，返回指定的页面中
//     *
//     */
//    @ExceptionHandler(value = {java.lang.NullPointerException.class})
//    public ModelAndView nullPointerException(Exception e){
//        ModelAndView mv = new ModelAndView();
//        mv.addObject("error",e.toString());
//        mv.setViewName("errorThree");
//        return mv;
//    }
//
//}


///**
// * 全局捕捉异常变量信息:方法二【缺陷：相对比上面那种方法，这里不能捕捉到具体错误信息，只能返回到指定的页面】
// * SimpleMappingExceptionResolver
// */
//@Configuration
//public class GlobalException {
//
//    @Bean
//    public SimpleMappingExceptionResolver getSimpleMappingExceptionResolver(){
//        SimpleMappingExceptionResolver smer = new SimpleMappingExceptionResolver();
//        Properties properties = new Properties();
//        //第一个参数：必须是捕捉异常的全称
//        //第二个参数：返回的页面
//        properties.setProperty("java.lang.ArithmeticException","errorTwo");
//        properties.setProperty("java.lang.NullPointerException","errorThree");
//        smer.setExceptionMappings(properties);
//        return smer;
//    }
//
//}

/**
 * 全局捕捉异常变量信息:方法三【推荐：结果同方法一】
 * 通过实现HandlerExceptionResolver接口做全局异常处理
 */
@Configuration
public class GlobalException implements HandlerExceptionResolver {


    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        ModelAndView mv = new ModelAndView();
        // 捕捉不同类型会跳转到不同页面，会返回报错信息到页面
        if(e instanceof ArithmeticException){
            mv.setViewName("errorTwo");
        }
        if(e instanceof NullPointerException){
            mv.setViewName("errorThree");
        }
        mv.addObject("error",e.toString());
        return mv;
    }
}
