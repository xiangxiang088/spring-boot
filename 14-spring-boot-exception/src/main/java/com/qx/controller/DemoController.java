package com.qx.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;

/**
 * exception异常处理方式一：默认处理方式
 *
 */
@Controller
public class DemoController {

    /**
     * str.length();  故意报错，会跳转到springboot自身带的异常处理页面，例如：
     * Whitelabel Error Page
     * This application has no explicit mapping for /error, so you are seeing this as a fallback.
     *
     * Mon Feb 03 23:35:05 CST 2020
     * There was an unexpected error (type=Internal Server Error, status=500).
     * No message available
     * @return
     */
    @RequestMapping("/show")
    public String showInfo(){
        String str = null;
        str.length();//空指针异常
        return "index";
    }

    /**
     * SpringBoot默认的处理异常机制：Spirngboot默认的已经提供了一套处理异常的机制。
     * 一旦程序中出现了异常spirngboot会像/error的url发送请求。在springboot中提供了一个叫BasicExceptionController
     * 来处理/error的请求，然后默认显示异常的页面来展示异常信息。
     * 如果我们需要将所有的异常统一跳转到自定义的错误提示，需要在src/main/resources/templates目录下面创建error.html
     * 的页面，注意：名称必须是error.html
     * @return
     */
    @RequestMapping("/show2")
    public String showInfo2(){
        int a = 10/0;//算数异常
        return "index";
    }

    /**
     * java.lang.ArithmeticException（捕捉算数异常）
     * 该方法是需要返回一个ModelAndView,用于捕捉异常和返回指定的错误页面
     *
     * Exception e：捕捉产生异常，注入这个方法中，然后将错误信息返回给前端提示
     */
    @ExceptionHandler(value={java.lang.ArithmeticException.class})
    public ModelAndView arithmeticException(Exception e){
        ModelAndView mv = new ModelAndView();
        mv.addObject("error",e.toString());
        mv.setViewName("errorTwo");
        return mv;
    }

    /**
     * java.lang.NullPointerException（捕捉空指针异常）
     * 该方法需要返回一个ModelAndView的页面：目的是专门捕捉空指针异常然后返回指定的页面中
     *
     * Exception e:捕捉异常，然后将异常注入该方法中，返回指定的页面中
     *
     */
    @ExceptionHandler(value = {java.lang.NullPointerException.class})
    public ModelAndView nullPointerException(Exception e){
        ModelAndView mv = new ModelAndView();
        mv.addObject("error",e.toString());
        mv.setViewName("errorThree");
        return mv;
    }

}
