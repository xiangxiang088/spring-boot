package com.qx.pojo;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * 角色
 */
@Entity
@Table(name="t_roles")
public class Roles {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "roleid")
    private Integer roleid;

    @Column(name="rolename")
    private String rolename;

    //对多一
    //mappedBy = "roles"  roles拥有一对多用户，就是一个角色拥有多个用户
    @OneToMany(mappedBy = "roles")
    private Set<Users> users = new HashSet<>();

    //多对多
    //(cascade = CascadeType.PERSIST) 开启级联操作
    //fetch = FetchType.EAGER 意思是hibernate延迟加载改为必须加载
    //@JoinTable：映射中间表
    //joinColumns：当前表中的主键关联中间表的外键字段
    //inverseJoinColumns：当前表中的主键关联中间表的外键字段，内表中外键字段
    @ManyToMany(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
    @JoinTable(name = "t_roles_menus",joinColumns = @JoinColumn(name="role_id"),inverseJoinColumns = @JoinColumn(name="menu_id"))
    private Set<Menus> menus = new HashSet<>();

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public Set<Users> getUsers() {
        return users;
    }

    public void setUsers(Set<Users> users) {
        this.users = users;
    }

    public Set<Menus> getMenus() {
        return menus;
    }

    public void setMenus(Set<Menus> menus) {
        this.menus = menus;
    }

    /**
     * 特别的，这里的users和menus不能toString,否则报错
     * @return
     */
    @Override
    public String toString() {
        return "Roles{" +
                "roleid=" + roleid +
                ", rolename='" + rolename + '\'' +
//                ", users=" + users +
//                ", menus=" + menus +
                '}';
    }
}
