package com.qx.pojo;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Menus实体类
 */
@Entity
@Table(name = "t_menus")
public class Menus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "merusid")
    private Integer menusid;

    @Column(name="menusname")
    private String menusname;

    @Column(name="menusurl")
    private String menusurl;

    @Column(name = "fatherid")
    private Integer fatherid;

    /**
     * 在四种关联关系OneToOne，OneToMany，ManyToOne和ManyToMany中，只有OneToOne、OneToMany和ManyToMany这三中关联关系有mappedBy属性。
     * 下面是mappedBy属性在java doc里边的解释：
     * the field that owns the relationship. Required unless the relationship is unidirectional.
     * 翻译成中文是：
     * 拥有关联关系的域，如果关系是单向的就不需要。
     * 那么什么叫拥有关联关系呢，可以这么认为，假设是双向一对一的话，那么拥有关系的这一方有建立、解除和更新与另一方关系的能力，而另一方没有，只能被动管理；在双向一对多和双向多对多中是一个意思。
     * 由于JoinTable和JoinColumn一般定义在拥有关系的这一端，而Hibernate又不让mappedBy跟JoinTable和JoinColumn定义在一起，所以mappedBy一定是定义在关系的被拥有方，the owned side，也就是跟定义JoinTable和JoinColumn互斥的一方，它的值指向拥有方中关于被拥有方的字段，可能是一个对象（OneToMany），也可能是一个对象集合（ManyToMany）。
     * 关系的拥有方负责关系的维护，在关系拥有方建立外键，所以JoinTable和JoinColumn都是定义在关系拥有方。
     * 另外mappedBy有一个功能就是，加入这个属性之后，可以避免自动生成中间表。
     * 最后，mappedBy=“xxx”，可以这么理解，mappedBy定义在关系的被拥有方，mappedBy定义所在的类（不管是单个还是集合的形式）在关系拥有者那一方的名称是“xxx”。
     */
    @ManyToMany(mappedBy = "menus")
    private Set<Roles> roles = new HashSet<>();

    public Integer getMenusid() {
        return menusid;
    }

    public void setMenusid(Integer menusid) {
        this.menusid = menusid;
    }

    public String getMenusname() {
        return menusname;
    }

    public void setMenusname(String menusname) {
        this.menusname = menusname;
    }

    public String getMenusurl() {
        return menusurl;
    }

    public void setMenusurl(String menusurl) {
        this.menusurl = menusurl;
    }

    public Integer getFatherid() {
        return fatherid;
    }

    public void setFatherid(Integer fatherid) {
        this.fatherid = fatherid;
    }

    public Set<Roles> getRoles() {
        return roles;
    }

    public void setRoles(Set<Roles> roles) {
        this.roles = roles;
    }

    /**
     * 特别的，这里的roles不能toString,否则报错
     * @return
     */
    @Override
    public String toString() {
        return "Menus{" +
                "menusid=" + menusid +
                ", menusname='" + menusname + '\'' +
                ", menusurl='" + menusurl + '\'' +
                ", fatherid=" + fatherid +
//                ", roles=" + roles +
                '}';
    }
}
