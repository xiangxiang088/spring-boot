package com.qx.dao;

import com.qx.pojo.Roles;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Repository @Query
 */
public interface RolesRepositoryQueryAnnotation extends Repository<Roles,Integer> {

    // 1.第一个问号默认注入第一个参数name
    @Query("from Roles where roleid = ?")
    List<Roles> queryByRoleidUseHQL(Integer roleid);


}
