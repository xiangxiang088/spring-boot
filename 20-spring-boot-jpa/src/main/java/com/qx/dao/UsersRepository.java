package com.qx.dao;

import com.qx.pojo.Users;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 参数一T:当前需要映射的实体
 * 参数二ID:当前映射的实体中的OID的类型
 */
public interface UsersRepository extends JpaRepository<Users,Integer> {
}
