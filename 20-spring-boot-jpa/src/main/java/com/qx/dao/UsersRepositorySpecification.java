package com.qx.dao;

import com.qx.pojo.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * JpaSpecificationExecutor定义接口
 */
public interface UsersRepositorySpecification extends JpaRepository<Users,Integer>, JpaSpecificationExecutor<Users> {
}
