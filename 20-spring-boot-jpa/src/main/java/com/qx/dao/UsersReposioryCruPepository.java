package com.qx.dao;

import com.qx.pojo.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * CrudRepository接口定义，主要是完成一些增删改查的操作。注意：CruRepository主要继承Repository的接口。
 */
public interface UsersReposioryCruPepository extends CrudRepository<Users,Integer> {
}

/**
 * 继承源码
 *
 * package org.springframework.data.repository;
 *
 * @org.springframework.data.repository.NoRepositoryBean
 * public interface CrudRepository <T, ID extends java.io.Serializable> extends org.springframework.data.repository.Repository<T,ID> {
 *     <S extends T> S save(S s);
 *
 *     <S extends T> java.lang.Iterable<S> save(java.lang.Iterable<S> iterable);
 *
 *     T findOne(ID id);
 *
 *     boolean exists(ID id);
 *
 *     java.lang.Iterable<T> findAll();
 *
 *     java.lang.Iterable<T> findAll(java.lang.Iterable<ID> iterable);
 *
 *     long count();
 *
 *     void delete(ID id);
 *
 *     void delete(T t);
 *
 *     void delete(java.lang.Iterable<? extends T> iterable);
 *
 *     void deleteAll();
 */
