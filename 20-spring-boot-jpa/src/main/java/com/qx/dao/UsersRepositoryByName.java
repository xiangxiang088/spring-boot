package com.qx.dao;

import com.qx.pojo.Users;
import org.springframework.data.repository.Repository;

import java.util.Date;
import java.util.List;

/**
 * Repository接口的方法名称命名查询
 */
public interface UsersRepositoryByName extends Repository<Users,Integer> {

    //1.方法的名称必须要遵循驼峰式命名规则。findBy(关键字)+属性名称（首字母要大写）+查询条件（首字母大写）
    List<Users> findByName(String name);

    //2.通过name和age去查询
    List<Users> findByNameAndAge(String name,Integer age);

    //3.通过name或者age去查询
    List<Users> findByNameOrAge(String name,Integer age);

    //4.模糊查询
    List<Users> findByNameLike(String name);

    //5.拓展：包含查询
    List<Users> findByNameContains(String name);

    //6.拓展：between...and...
    List<Users> findByCreateDateBetween(Date startDate,Date endDate);

    //7.拓展：查询什么时间之后数据
    List<Users> findByCreateDateAfter(Date createDate);

    //8.拓展：查询什么时间之前数据
    List<Users> findByCreateDateBefore(Date createDate);

    //9.拓展：模糊查询不等于
    List<Users> findByNameNotLike(String name);

    //10.拓展：模糊查询不等于
    List<Users> findByNameIsNotLike(String name);

}
