package com.qx.dao;

import com.qx.pojo.Users;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Repository @Query
 */
public interface UsersRepositoryQueryAnnotation extends Repository<Users,Integer> {

    // 1.第一个问号默认注入第一个参数name
    @Query("from Users where name = ?")
    List<Users> queryByNameUseHQL(String name);

    // 2.自己写sql语句,nativeQuery = 默认为false,当设置为true时，告诉hibernate不用再转换标准sql语句
    @Query(value = "select * from t_users where name = ?",nativeQuery = true)
    List<Users> queryByNameUseSQL(String name);

    //3.修改
    @Query("update Users set name=? where id=?")
    @Modifying //需要执行一个更新操作
    int updateUsersNameById(String name,Integer id);

    //4.查询
    @Query("from Users where roles_id=?")
    List<Users> queryByRolesIdUseHQL(Integer rolesid);

}
