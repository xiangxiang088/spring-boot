package com.qx.dao;

import com.qx.pojo.Users;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * 定义接口：UsersRepositoryPaginAndSorting
 */
public interface UsersRepositoryPaginAndSorting extends PagingAndSortingRepository<Users,Integer> {

}
