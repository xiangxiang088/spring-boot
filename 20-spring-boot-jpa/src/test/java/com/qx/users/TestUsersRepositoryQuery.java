package com.qx.users;

import com.qx.App;
import com.qx.dao.UsersRepositoryQueryAnnotation;
import com.qx.pojo.Users;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Query测试方法
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = App.class)
public class TestUsersRepositoryQuery {

    @Autowired
    private UsersRepositoryQueryAnnotation queryAnnotation;

    /**
     * 1.Repository--@Query测试
     */
    @Test
    public void testUsersRepositoryQueryByNameUseHQL(){
        List<Users> list = this.queryAnnotation.queryByNameUseHQL("张三");
        for(Users users:list){
            System.out.println("结果："+users);
            //结果：Users{id=1, name='张三', age=24, address='珠江新城', createDate=null}
        }
    }

    /**
     * 2.Repository--@Query测试
     */
    @Test
    public void testUsersQueryByNameUseSQL(){
        List<Users> list = this.queryAnnotation.queryByNameUseSQL("李四");
        for(Users users:list){
            System.out.println("结果："+users);
            //结果：Users{id=2, name='李四', age=26, address='广州南站', createDate=null}
        }
    }

    /**
     * 3.Repository--@Query修改
     */
    @Test
    @Transactional //@Transactional与@Test一起使用时，事物是自动回滚的。
    @Rollback(false) //取消自动回滚
    public void testUsersUpdateById(){
        this.queryAnnotation.updateUsersNameById("张三",1);
    }


}
