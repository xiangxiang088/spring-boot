package com.qx.users;

import com.qx.App;
import com.qx.pojo.Users;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.List;

/**
 * JpaSpecificationExecutor测试类
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = App.class)
public class TestUsersJpaSpecificationExecutor {

    @Autowired
    private JpaSpecificationExecutor jpaSpecificationExecutor;

    //(1)排序查询：单条件
    @Test
    public void sortingRepository(){
        /**
         * Specification<Users>用于封装查询条件
         * 抽象实现类，少去维护一个class
         */
        Specification<Users> spec = new Specification<Users>() {
            /**
             * Predicate:封装了单个查询条件
             * @param root:查询对象的属性的封装
             * @param criteriaQuery：封装了我们要执行的查询中的各个部分的信息，例如：selete from order group by
             * @param criteriaBuilder：查询条件的构造器。定义不同的查询条件
             * @return Predicate
             */
            @Override
            public Predicate toPredicate(Root<Users> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                // where name = "张三"
                /**
                 * 参数一：查询的条件
                 * 参数二：条件的值
                 */
                Predicate pre = criteriaBuilder.equal(root.get("name"),"张三");
                return pre;
            }
        };

        List<Users> list = this.jpaSpecificationExecutor.findAll(spec);
        for (Users users:list){
            System.out.println(users);
        }
        //Users{id=1, name='张三', age=24, address='珠江新城', createDate=null}
    }

    //(2)排序查询：多条件
    @Test
    public void sortingRepositoryMore(){
        /**
         * Specification<Users>用于封装查询条件
         * 抽象实现类，少去维护一个class
         */
        Specification<Users> spec = new Specification<Users>() {
            /**
             * Predicate:封装了单个查询条件
             * @param root:查询对象的属性的封装
             * @param criteriaQuery：封装了我们要执行的查询中的各个部分的信息，例如：selete from order group by
             * @param criteriaBuilder：查询条件的构造器。定义不同的查询条件
             * @return Predicate
             */
            @Override
            public Predicate toPredicate(Root<Users> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                // where name = "张三" and age=24
                List<Predicate> list = new ArrayList<>();
                list.add(criteriaBuilder.equal(root.get("name"),"张三"));
                list.add(criteriaBuilder.equal(root.get("age"),24));
                //list转化数组
                Predicate[] arr = new Predicate[list.size()];
                //绑定两个条件的关系，比如：and or like .....
                return criteriaBuilder.and(list.toArray(arr));
            }
        };

        List<Users> list = this.jpaSpecificationExecutor.findAll(spec);
        for (Users users:list){
            System.out.println(users);
        }
//        Hibernate: select *_ from t_users users0_ where users0_.name=? and users0_.age=24
//        Users{id=1, name='张三', age=24, address='珠江新城', createDate=null}
    }


    //(3)排序查询：多条件方式二：简化版
    @Test
    public void sortingRepositoryThree(){
        /**
         * Specification<Users>用于封装查询条件
         * 抽象实现类，少去维护一个class
         */
        Specification<Users> spec = new Specification<Users>() {
            /**
             * Predicate:封装了单个查询条件
             * @param root:查询对象的属性的封装
             * @param criteriaQuery：封装了我们要执行的查询中的各个部分的信息，例如：selete from order group by
             * @param cb：查询条件的构造器。定义不同的查询条件
             * @return Predicate
             */
            @Override
            public Predicate toPredicate(Root<Users> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {

                // where name = "张三" and age=24
                // return cb.and(cb.equal(root.get("name"),"张三"),cb.equal(root.get("age"),24));

                // where name = "张三" or age=24
                //return cb.or(cb.equal(root.get("name"),"张三"),cb.equal(root.get("age"),24));

                //(name = "张三" and age=24) or id=2
                //注意：下面分开看就很容易看清楚了
                //cb.and()
                //cb.equal(root.get("name"),"张三"),cb.equal(root.get("age"),24)
                //cb.equal(root.get("id"),2)
                return cb.or(cb.and(cb.equal(root.get("name"),"张三"),cb.equal(root.get("age"),24)),cb.equal(root.get("id"),2));

            }
        };
        //排序
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        //至于分页，跟上一节课相同
        List<Users> list = this.jpaSpecificationExecutor.findAll(spec,sort);
        for (Users users:list){
            System.out.println(users);
        }
        /*Hibernate: select *_ from t_users users0_ where users0_.name=? and users0_.age=24
        Users{id=1, name='张三', age=24, address='珠江新城', createDate=null}*/

        /*Hibernate: select *_ from t_users users0_ where users0_.name=? and users0_.age=24
        Users{id=1, name='张三', age=24, address='珠江新城', createDate=null}*/

        /*Hibernate: select *_ from t_users users0_ where users0_.name=? and users0_.age=24 or users0_.id=2
        Users{id=1, name='张三', age=24, address='珠江新城', createDate=null}
        Users{id=2, name='李四', age=26, address='广州南站', createDate=null}*/
    }


}
