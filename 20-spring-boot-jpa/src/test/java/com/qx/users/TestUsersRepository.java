package com.qx.users;

import com.qx.App;
import com.qx.UtilsDate;
import com.qx.dao.UsersRepository;
import com.qx.dao.UsersRepositoryByName;
import com.qx.pojo.Users;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 * 测试类
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = App.class)
public class TestUsersRepository {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private UsersRepositoryByName usersRepositoryByName;

    @Test
    public void testAddUsers(){
        Users users = new Users();
        users.setName("孙悟空");
        users.setAge(100);
        users.setAddress("花果山");
        this.usersRepository.save(users);
    }

    /**
     * 1.Repository--方法名称命名测试
     */
    @Test
    public void testFindByName(){
        List<Users> list = this.usersRepositoryByName.findByName("张三");
        for (Users users:list){
            System.out.println(users);//Users{id=1, name='张三', age=24, address='珠江新城'}
        }
    }

    /**
     *2.Repository-通过name和age去查询测试
     */
    @Test
    public void testFindByNameAndAge(){
        List<Users> list = this.usersRepositoryByName.findByNameAndAge("张三",24);
        for(Users users:list){
            System.out.println("结果："+users);//Users{id=1, name='张三', age=24, address='珠江新城'}
        }
    }

    /**
     *3.Repository-通过name或者age去查询测试
     */
    @Test
    public void testFindByNameOrAge(){
        List<Users> list = this.usersRepositoryByName.findByNameOrAge("张三",100);
        for(Users users:list){
            System.out.println("结果："+users);
        }
//        结果：Users{id=1, name='张三', age=24, address='珠江新城'}
//        结果：Users{id=8, name='孙悟空', age=100, address='花果山'}
    }

    /**
     * 4.Repository-通过方法命名模糊查询
     */
    @Test
    public void testFindByNameLike(){
//        List<Users> list = this.usersRepositoryByName.findByNameLike("啊%");
//        for(Users users:list){
//            System.out.println("结果："+users);
//        }
//        结果：Users{id=6, name='啊八', age=26, address='九寨沟'}
//        结果：Users{id=7, name='啊九', age=26, address='人民代表大堂'}

//        List<Users> list = this.usersRepositoryByName.findByNameLike("%三脚猫");
//        for(Users users:list){
//            System.out.println("结果："+users);
//        }
//        结果：Users{id=9, name='啊大三脚猫', age=22, address='倚天独龙记'}
//        结果：Users{id=10, name='啊二三脚猫', age=28, address='倚天独龙记'}

        List<Users> list = this.usersRepositoryByName.findByNameLike("%啊%");
        for(Users users:list){
            System.out.println("结果："+users);
        }
//        结果：Users{id=6, name='啊八', age=26, address='九寨沟'}
//        结果：Users{id=7, name='啊九', age=26, address='人民代表大堂'}
//        结果：Users{id=9, name='啊大三脚猫', age=22, address='倚天独龙记'}
//        结果：Users{id=10, name='啊二三脚猫', age=28, address='倚天独龙记'}
    }

    /**
     * 5.Repository-通过包含查询
     */
    @Test
    public void testFindByNameContrains(){
        List<Users> list = this.usersRepositoryByName.findByNameContains("啊");
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
//        Users{id=6, name='啊八', age=26, address='九寨沟'}
//        Users{id=7, name='啊九', age=26, address='人民代表大堂'}
//        Users{id=9, name='啊大三脚猫', age=22, address='倚天独龙记'}
//        Users{id=10, name='啊二三脚猫', age=28, address='倚天独龙记'}
    }


    /**
     * 6.Repository-between---and---
     */
    @Test
    public void testFindNameByBetween(){
        List<Users> list = this.usersRepositoryByName.findByCreateDateBetween(UtilsDate.strToDateLong("2020-02-08 12:12:24"),new Date());
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
//        Users{id=9, name='啊大三脚猫', age=22, address='倚天独龙记', createDate=2020-02-10 14:40:57.0}
//        Users{id=10, name='啊二三脚猫', age=28, address='倚天独龙记', createDate=2020-02-11 14:40:52.0}
    }

    /**
     * 9.Repository-模糊查询NotLike
     */
    @Test
    public void testFindNameByNotLike(){
        List<Users> list = this.usersRepositoryByName.findByNameNotLike("%啊%");
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
//        Hibernate: select * from t_users users0_ where users0_.name not like ?
//        Users{id=1, name='张三', age=24, address='珠江新城', createDate=null}
//        Users{id=2, name='李四', age=26, address='广州南站', createDate=null}
//        Users{id=3, name='王五', age=22, address='华发商都', createDate=null}
//        Users{id=4, name='赵六', age=26, address='布达拉空', createDate=null}
//        Users{id=5, name='王七', age=26, address='桂林', createDate=null}
//        Users{id=8, name='孙悟空', age=100, address='花果山', createDate=2020-02-01 14:41:02.0}

        List<Users> list2 = this.usersRepositoryByName.findByNameNotLike("啊");
        for(int i=0;i<list2.size();i++){
            System.out.println(list2.get(i));
        }
        //结果：所有数据

    }

    /**
     * 10.Repository-模糊查询isNotLike
     */
    @Test
    public void testFindNameByIsNotLike(){
        List<Users> list = this.usersRepositoryByName.findByNameIsNotLike("%啊%");
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
//        Hibernate: select * from t_users users0_ where users0_.name not like ?
//        Users{id=1, name='张三', age=24, address='珠江新城', createDate=null}
//        Users{id=2, name='李四', age=26, address='广州南站', createDate=null}
//        Users{id=3, name='王五', age=22, address='华发商都', createDate=null}
//        Users{id=4, name='赵六', age=26, address='布达拉空', createDate=null}
//        Users{id=5, name='王七', age=26, address='桂林', createDate=null}
//        Users{id=8, name='孙悟空', age=100, address='花果山', createDate=2020-02-01 14:41:02.0}

        List<Users> list2 = this.usersRepositoryByName.findByNameIsNotLike("啊");
        for(int i=0;i<list2.size();i++){
            System.out.println(list2.get(i));
        }
        //结果：所有数据
//        Hibernate: select * from t_users users0_ where users0_.name not like ?
//        Users{id=1, name='张三', age=24, address='珠江新城', createDate=null}
//        Users{id=2, name='李四', age=26, address='广州南站', createDate=null}
//        Users{id=3, name='王五', age=22, address='华发商都', createDate=null}
//        Users{id=4, name='赵六', age=26, address='布达拉空', createDate=null}
//        Users{id=5, name='王七', age=26, address='桂林', createDate=null}
//        Users{id=6, name='啊八', age=26, address='九寨沟', createDate=null}
//        Users{id=7, name='啊九', age=26, address='人民代表大堂', createDate=null}
//        Users{id=8, name='孙悟空', age=100, address='花果山', createDate=2020-02-01 14:41:02.0}
//        Users{id=9, name='啊大三脚猫', age=22, address='倚天独龙记', createDate=2020-02-10 14:40:57.0}
//        Users{id=10, name='啊二三脚猫', age=28, address='倚天独龙记', createDate=2020-02-11 14:40:52.0}

    }


}
