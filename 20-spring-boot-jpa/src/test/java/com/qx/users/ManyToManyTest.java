package com.qx.users;

import com.qx.App;
import com.qx.dao.RolesJpaRepository;
import com.qx.pojo.Menus;
import com.qx.pojo.Roles;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;
import java.util.UUID;

/**
 * 多对对关联关系测试
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = App.class)
public class ManyToManyTest {

    @Autowired
    private RolesJpaRepository rolesJpaRepository;

    /**
     * 添加测试
     */
    @Test
    public void testSave(){
        //创建角色
        Roles roles = new Roles();
        roles.setRolename("技术总监");

        //创建菜单
        Menus menus = new Menus();
        menus.setFatherid(0);//顶级菜单，默认0就OK
        menus.setMenusname("***收银管理系统***");
        menus.setMenusurl("");

        Menus menus2 = new Menus();
        menus2.setFatherid(1);//一级菜单，默认1就OK
        menus2.setMenusname("会员管理");
        menus2.setMenusurl("");

        Menus menus3 = new Menus();
        menus3.setFatherid(3);//二级菜单，一般都是UUID,防止重复  UUID.randomUUID().toString().replace("-", "").toLowerCase()
        menus3.setMenusname("会员查询");
        menus3.setMenusurl("http://www.baidu.com");

        //关联
        roles.getMenus().add(menus);
        roles.getMenus().add(menus2);
        roles.getMenus().add(menus3);
        menus.getRoles().add(roles);
        menus2.getRoles().add(roles);
        menus3.getRoles().add(roles);

        //保存
        this.rolesJpaRepository.save(roles);

        //Hibernate: insert into t_roles (rolename) values (?)
        //Hibernate: insert into t_menus (fatherid, menusname, menusurl) values (?, ?, ?)
        //Hibernate: insert into t_menus (fatherid, menusname, menusurl) values (?, ?, ?)
        //Hibernate: insert into t_menus (fatherid, menusname, menusurl) values (?, ?, ?)
        //Hibernate: insert into t_roles_menus (role_id, menu_id) values (?, ?)
        //Hibernate: insert into t_roles_menus (role_id, menu_id) values (?, ?)
        //Hibernate: insert into t_roles_menus (role_id, menu_id) values (?, ?)
    }


    /**
     * 查询：通过角色ID去查询所有菜单
     * hibernate:默认延迟加载，要去后台改为必须加载！
     *
     * 如果不必须加载会报错：failed to lazily initialize a collection of role: com.qx.pojo.Roles.menus, could not initialize proxy - no Session
     * 这个是懒加载异常，就是在查询时没有加载关联表的对象，你读取这个关联对象的时候，hibernate的session已经关闭，所以无法获取对象。
     */
    @Test
    public void testFind(){
        Roles roles = this.rolesJpaRepository.findOne(7);
        System.out.println(roles.getRolename());
        System.out.println(roles);
        Set<Menus> set = roles.getMenus();
        for(Menus menus:set){
            System.out.println(menus);
        }
        /*SELECT
        roles0_.roleid AS roleid1_1_0_,
                roles0_.rolename AS rolename2_1_0_,
        menus1_.role_id AS role_id1_2_1_,
                menus2_.merusid AS menu_id2_2_1_,
        menus2_.merusid AS merusid1_0_2_,
                menus2_.fatherid AS fatherid2_0_2_,
        menus2_.menusname AS menusnam3_0_2_,
                menus2_.menusurl AS menusurl4_0_2_
        FROM
        t_roles roles0_
        LEFT OUTER JOIN t_roles_menus menus1_ ON roles0_.roleid = menus1_.role_id
        LEFT OUTER JOIN t_menus menus2_ ON menus1_.menu_id = menus2_.merusid
        WHERE
        roles0_.roleid =7*/
//        技术总监
//        Roles{roleid=7, rolename='技术总监'}
//        Menus{menusid=5, menusname='会员管理', menusurl='', fatherid=1}
//        Menus{menusid=7, menusname='会员查询', menusurl='http://www.baidu.com', fatherid=3}
//        Menus{menusid=6, menusname='***收银管理系统***', menusurl='', fatherid=0}
    }

}
