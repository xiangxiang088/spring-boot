package com.qx.users;

import com.qx.App;
import com.qx.dao.UsersReposioryCruPepository;
import com.qx.pojo.Users;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 * CrudPository-测试类
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = App.class)
public class TestUsersCrudPository {

    @Autowired
    private UsersReposioryCruPepository crudRepository;

    //1.新增
    @Test
    public void testCrudPositorySave(){
        Users users = new Users();
        users.setName("猪八戒");
        users.setAge(5000);
        users.setAddress("天庭天蓬元帅");
        users.setCreateDate(new Date());
        Users u = this.crudRepository.save(users);
    }

    //2.修改（跟新增同用一个方法）
    @Test
    public void testCrudPositoryUpdate(){
        Users users = new Users();
        users.setId(12);
        users.setName("猪八戒666");
        users.setAge(5000);
        users.setAddress("天庭天蓬元帅");
        users.setCreateDate(new Date());
        Users u = this.crudRepository.save(users);
    }

    //3.单个查询
    @Test
    public void testCrudPositoryFindOne(){
        Users users = this.crudRepository.findOne(12);
        System.out.println(users);
        System.out.println(users.getName());
        //Users{id=12, name='猪八戒666', age=5000, address='天庭天蓬元帅', createDate=2020-02-11 17:39:26.0}
    }

    //4.查询所有
    @Test
    public void testCrudPositoryfindAll(){
        List<Users> list = (List<Users>)this.crudRepository.findAll();
        for(Users users:list)
            System.out.println(users);
//        Users{id=1, name='张三', age=24, address='珠江新城', createDate=null}
//        Users{id=2, name='李四', age=26, address='广州南站', createDate=null}
//        Users{id=3, name='王五', age=22, address='华发商都', createDate=null}
//        Users{id=4, name='赵六', age=26, address='布达拉空', createDate=null}
//        Users{id=5, name='王七', age=26, address='桂林', createDate=null}
//        Users{id=6, name='啊八', age=26, address='九寨沟', createDate=null}
//        Users{id=7, name='啊九', age=26, address='人民代表大堂', createDate=null}
//        Users{id=8, name='孙悟空', age=100, address='花果山', createDate=2020-02-01 14:41:02.0}
//        Users{id=9, name='啊大三脚猫', age=22, address='倚天独龙记', createDate=2020-02-10 14:40:57.0}
//        Users{id=10, name='啊二三脚猫', age=28, address='倚天独龙记', createDate=2020-02-11 14:40:52.0}
//        Users{id=11, name='猪八戒', age=5000, address='天庭天蓬元帅', createDate=2020-02-11 17:33:37.0}
//        Users{id=12, name='猪八戒666', age=5000, address='天庭天蓬元帅', createDate=2020-02-11 17:39:26.0}
    }

    //5.删除
    @Test
    public void testCrudPository(){
        this.crudRepository.delete(11);
    }
}
