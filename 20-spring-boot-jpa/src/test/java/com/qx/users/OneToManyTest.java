package com.qx.users;

import com.qx.App;
import com.qx.dao.*;
import com.qx.pojo.Roles;
import com.qx.pojo.Users;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 * OneToMany测试
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = App.class)
public class OneToManyTest {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private RolesJpaRepository rolesJpaRepository;
    @Autowired
    private RolesRepositoryQueryAnnotation rolesRepositoryQueryAnnotation;
    @Autowired
    private UsersRepositoryByName usersRepositoryByName;
    @Autowired
    private UsersRepositoryQueryAnnotation usersRepositoryQueryAnnotation;

    /**
     * 一对多关联关系：添加
     */
    @Test
    public void testSave(){
        //1.创建一个用户
        Users users = new Users();
        users.setAddress("广西");
        users.setAge(24);
        users.setName("覃老师");
        users.setCreateDate(new Date());
        //2.创建一个角色
        Roles roles = new Roles();
        roles.setRolename("店长");
        //3.关联
        roles.getUsers().add(users);
        users.setRoles(roles);
        //4.保存
        this.usersRepository.save(users);
//        Hibernate: insert into t_roles (rolename) values (?)
//        Hibernate: insert into t_users (address, age, create_date, name, roles_id) values (?, ?, ?, ?, ?)
//        系统会自动去数据库添加t_roles表
    }

    /**
     * 一对多关联关系：查询【通过用户查询角色】
     */
    @Test
    public void testFind(){
        Users users = this.usersRepository.findOne(14);
        System.out.println(users);
        Roles roles = users.getRoles();
        System.out.println(roles.getRolename());
//        Users{id=13, name='小三', age=23, address='天津', createDate=null}
//        管理员
    }

    /**
     * 一对多关联关系：查询【通过角色查询用户】
     */
    @Test
    public void testFindRoles(){
        List<Roles> list = this.rolesRepositoryQueryAnnotation.queryByRoleidUseHQL(3);
        System.out.println(list.get(0).getRolename());
        System.out.println(list.get(0).getRoleid());
        List<Users> usersList = this.usersRepositoryQueryAnnotation.queryByRolesIdUseHQL(list.get(0).getRoleid());
        for(Users users:usersList){
            System.out.println(users);
        }
    }

}
