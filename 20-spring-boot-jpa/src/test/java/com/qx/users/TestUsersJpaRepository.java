package com.qx.users;

import com.qx.App;
import com.qx.dao.UsersRepository;
import com.qx.pojo.Users;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * JpaRepository测试类
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = App.class)
public class TestUsersJpaRepository {

    @Autowired
    private UsersRepository usersRepository;

    //排序查询
    @Test
    public void sortingRepository(){
        //设置排序方式：第一个参数：降序/升序; 第二个参数：排序属性
        Sort.Order order = new Sort.Order(Sort.Direction.DESC,"id");
        Sort sort = new Sort(order);
        List<Users> list = this.usersRepository.findAll(sort);
        for (Users users:list){
            System.out.println(users);
        }
//        Users{id=4, name='赵六', age=26, address='布达拉空', createDate=null}
//        Users{id=3, name='王五', age=22, address='华发商都', createDate=null}
//        Users{id=2, name='李四', age=26, address='广州南站', createDate=null}
//        Users{id=1, name='张三', age=24, address='珠江新城', createDate=null}
    }

    //分页查询
    @Test
    public void pagingRepository(){
        //new PageRequest(0,2) 0:表示显示第几页，2：表示显示多少条数据
        Pageable pageable = new PageRequest(0,5);
        Page<Users> page = this.usersRepository.findAll(pageable);
        System.out.println("总条数："+page.getTotalElements());
        System.out.println("总页数："+page.getTotalPages());
        List<Users> list = page.getContent();
        for (Users users :list){
            System.out.println(users);
        }
//        Hibernate: select count(users0_.id) as col_0_0_ from t_users users0_
//        总条数：11
//        总页数：3
//        Users{id=1, name='张三', age=24, address='珠江新城', createDate=null}
//        Users{id=2, name='李四', age=26, address='广州南站', createDate=null}
//        Users{id=3, name='王五', age=22, address='华发商都', createDate=null}
//        Users{id=4, name='赵六', age=26, address='布达拉空', createDate=null}
//        Users{id=5, name='王七', age=26, address='桂林', createDate=null}
    }


    //排序和分页
    @Test
    public void testPagingAndSortingRepository(){
        Sort.Order order = new Sort.Order(Sort.Direction.DESC,"id");
        Sort sort = new Sort(order);
        Pageable pageable = new PageRequest(0,3,sort);
        Page<Users> page = this.usersRepository.findAll(pageable);
        System.out.println("总条数："+page.getTotalElements());
        System.out.println("总页数："+page.getTotalPages());
        List<Users> list = page.getContent();
        for(Users user:list){
            System.out.println(user);
        }
    }
//    总条数：11
//    总页数：4
//    Users{id=12, name='猪八戒666', age=5000, address='天庭天蓬元帅', createDate=2020-02-11 17:39:26.0}
//    Users{id=10, name='啊二三脚猫', age=28, address='倚天独龙记', createDate=2020-02-11 14:40:52.0}
//    Users{id=9, name='啊大三脚猫', age=22, address='倚天独龙记', createDate=2020-02-10 14:40:57.0}
}
