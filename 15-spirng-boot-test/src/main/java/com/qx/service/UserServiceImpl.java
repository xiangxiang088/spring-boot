package com.qx.service;

import com.qx.dao.UserDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl {

    @Autowired
    private UserDaoImpl dao;

    public void saveUser(){
        dao.saveUser();
    }

}
