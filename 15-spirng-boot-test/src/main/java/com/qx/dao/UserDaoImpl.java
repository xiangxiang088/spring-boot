package com.qx.dao;

import org.springframework.stereotype.Repository;

/**
 * @Repository、@Service、@Controller 和 @Component 将类标识为Bean
 *
 * Spring 自 2.0 版本开始，陆续引入了一些注解用于简化 Spring 的开发。@Repository注解便属于最先引入的一批，
 * 它用于将数据访问层 (DAO 层 ) 的类标识为 Spring Bean。具体只需将该注解标注在 DAO类上即可。
 * 同时，为了让 Spring 能够扫描类路径中的类并识别出 @Repository 注解，需要在 XML 配置文件中启用Bean 的自动扫描功能，
 * 这可以通过<context:component-scan/>实现。
 * ————————————————
 * 版权声明：本文为CSDN博主「偏爱大叔的小仙女」的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。
 * 原文链接：https://blog.csdn.net/qq_40943786/article/details/80966170
 */
@Repository
public class UserDaoImpl {


    public void saveUser(){
        System.out.println("--------------savaUser-------------------");
    }
}
