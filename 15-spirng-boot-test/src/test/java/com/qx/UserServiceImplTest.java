package com.qx;

import com.qx.service.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * SpringBoot测试类
 *
 * (1)@RunWith:启动器
 * (2)SpringJUnit4ClassRunner.class:让junit和spring环境进行整合
 * (3)@SpringBootTest(classes = {App.class}): 1.当前类为SpringBoot测试类；2.加载SpringBoot启动类，运行项目
 *
 *   以前springmvc的写法是：
 *   junit 与 spring 整合
 *   @Contextconfiguartion("classpath:applicationContext.xml")
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {App.class})
public class UserServiceImplTest {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Test
    public void saveUserTest(){
        this.userServiceImpl.saveUser();
    }
}
