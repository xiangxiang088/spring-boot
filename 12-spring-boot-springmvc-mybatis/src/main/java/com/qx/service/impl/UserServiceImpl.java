package com.qx.service.impl;

import com.qx.mapper.UsersMapper;
import com.qx.pojo.Users;
import com.qx.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UsersService {

    @Autowired
    private UsersMapper usersMapper;

    /**
     * 添加用户
     * @param users
     */
    @Override
    public void addUser(Users users) {
        this.usersMapper.insertUser(users);
    }

    /**
     * 查询所有用户
     * @return List<Users>
     */
    @Override
    public List<Users> findAllUser() {
        return this.usersMapper.selectAllUser();
    }

    /**
     * 通过id查询用户信息
     * @param id
     * @return users
     */
    @Override
    public Users findUserById(Integer id) {
        return this.usersMapper.selectUserById(id);
    }

    /**
     * 修改用户信息
     * @param users
     */
    @Override
    public void updUsers(Users users) {
        this.usersMapper.updateUserById(users);
    }

    /**
     * 删除用户信息
     * @param id
     */
    @Override
    public void delUser(Integer id) {
        this.usersMapper.deleteUserById(id);
    }


}
