package com.qx.service;

import com.qx.pojo.Users;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * service接口
 */
public interface UsersService {

    void addUser(Users users);

    List<Users> findAllUser();

    Users findUserById(Integer id);

    void updUsers(Users users);

    void delUser(Integer id);

}
