package com.qx.controller;

import com.qx.pojo.Users;
import com.qx.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UsersService usersService;

    /**
     * 1.页面跳转:thymeleaf里面的方法
     * 例如：http://127.0.0.1:8080/users/input
     * @return
     */
    @RequestMapping("/{page}")
    public String showPage(@PathVariable String page){ //@PathVariable是spring3.0的一个新功能：接收请求路径中占位符的值
        return page;
    }

    /**
     * 2.添加用户
     *
     */
    @RequestMapping("/addUser")
    public String addUser(Users users){
        this.usersService.addUser(users);
        return "ok";
    }

    /**
     * 3.查询所有用户
     */
    @RequestMapping("/findAllUser")
    public String findAllUser(Model model){
        List<Users> list = this.usersService.findAllUser();
        model.addAttribute("list",list);
        return "showAllUser";
    }

    /**
     * 4.通过id查询用户
     */
    @RequestMapping("/findUserById")
    public String findUserById(Integer id,Model model){
        Users user = this.usersService.findUserById(id);
        model.addAttribute("user",user);
        return "updateUser";
    }

    /**
     * 5.修改用户信息功能
     */
    @RequestMapping("/updateUser")
    public String updateUser(Users users){
        this.usersService.updUsers(users);
        return "ok";
    }

    /**
     * 6.删除用户信息
     */
    @RequestMapping("/delUser")
    public String delUser(Integer id){
        this.usersService.delUser(id);
        return "ok"; //return "redirect:/users/findUserAll";  这个直接返回：3.查询所有用户
    }

}
