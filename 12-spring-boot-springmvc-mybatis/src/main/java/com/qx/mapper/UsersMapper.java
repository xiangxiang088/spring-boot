package com.qx.mapper;

import com.qx.pojo.Users;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Mapper接口
 * 注意：
 * 1.映射配置文件，必须跟接口名相同
 * 2.通过查阅相关资料发现，IDEA对xml文件处理的方式不同，在Eclipse中到dao文件与mapper.xml文件只要在同一级目录下即可，
 * 但是在IDEA中，mapper.xml要放在resources目录下，且还有一个关键的地方是什么吧？就是resources下不能创建package，
 * 只能创建Directory，文件夹下面要想创建文件夹，就不能以(.)的形式，例如mapper.account，在src目录下可以创建package这样连着写包名，
 * 但是在资源文件夹下面就不能这样写了，这样写的话，就相当于这个文件夹的名称是“mapper.account”，而不是mapper文件夹下的account文件夹。
 */
@Mapper
public interface UsersMapper {

    void insertUser(Users users);

    List<Users> selectAllUser();

    Users selectUserById(Integer id);

    void updateUserById(Users users);

    void deleteUserById(Integer id);
}
