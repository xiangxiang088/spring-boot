package com.qx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot启动器
 *
 */
@SpringBootApplication
@MapperScan("com.qx.mapper") //MapperScan 用于扫描Mybatis的mapping接口，根据接口代理生成对象
public class App {
    public static void main( String[] args ) {
        SpringApplication.run(App.class,args);
    }
}
