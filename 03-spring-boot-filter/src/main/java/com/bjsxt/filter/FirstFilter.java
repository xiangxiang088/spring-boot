package com.bjsxt.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * SpringBoot整合Filter方式一：通过注解扫描
 * <filter>
 *     <filter-name>FirstFilter</filter-name>
 *     <filter-class>com.bjsxt.filter.FirstFilter</filter-class>
 * </filter>
 * <filter-mapping>
 *     <filter-name>FirstFilter</filter-name>
 *     <filter-pattern>/first</filter-pattern>
 * </filter-mapping>
 */
//@WebFilter(filterName = "FirstFilter",urlPatterns ={".do",".jsp"} )  //拦截多个请求filter专用，urlPatterns=里面是一个数组，逗号隔开
@WebFilter(filterName = "FirstFilter",urlPatterns = "/first")          //拦截单个请求filter,urlPatterns=里面是一个字符串即可
public class FirstFilter implements Filter {

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("----------------进入-FirstFile----------------------------");
        filterChain.doFilter(servletRequest,servletResponse);//放行内容
        System.out.println("----------------离开-FirstFile----------------------------");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
}
