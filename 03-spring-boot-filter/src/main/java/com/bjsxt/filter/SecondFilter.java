package com.bjsxt.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * Spring-boot整合filter方法二：通过方法
 */
public class SecondFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("----------------进入-SecondFile----------------------------");
        filterChain.doFilter(servletRequest,servletResponse);//放行内容
        System.out.println("----------------离开-SecondFile----------------------------");
    }

    @Override
    public void destroy() {

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
}
