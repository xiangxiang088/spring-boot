package com.qx;

import com.qx.pojo.Users;
import org.apache.catalina.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Spring Data Redis测试
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = App.class)
public class RedisTest {

    @Autowired
    private RedisTemplate<String,Object> redisTemplate; //注意：如果RedisTemplate<String,String>则返回Stirng

    //------------------------（1）添加一个redis值---------------------------------------------
    /**
     * 添加一个redis值
     */
    @Test
    public void addKeyTest(){
        this.redisTemplate.opsForValue().set("sex","女");
    }
    /**
     * 获取一个redis的值
     */
    @Test
    public void findKeyTest(){
        String value = (String)this.redisTemplate.opsForValue().get("sex");
        System.out.println(value);
    }


    //------------------------（2）添加一个users对象---------------------------------------------
    /**
     * 添加一个users对象
     */
    @Test
    public void testSetUsers(){
        Users users = new Users();
        users.setId(1);
        users.setName("张三");
        users.setAge(24);

        //this.redisTemplate.opsForValue().set("users",users);    com.qx.pojo.Users cannot be cast to java.lang.String

        //Cannot serialize; nested exception is org.springframework.core.serializer.support.SerializationFailedException: Failed to serialize object using DefaultSerializer; nested exception is java.lang.IllegalArgumentException: DefaultSerializer requires a Serializable payload but received an object of type [com.qx.pojo.Users]
        //报错原因：Users没有实现Serializable
        this.redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());//重新设置序列化器
        this.redisTemplate.opsForValue().set("users",users);

    }
    /**
     * 获取一个Users对象
     */
    @Test
    public void testGetUsers(){
        this.redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());//重新设置序列化器，反序列化，不然会取二进制内容
        Users users = (Users)this.redisTemplate.opsForValue().get("users");
        System.out.println(users);//Users{id=1, name='张三', age=24}
    }


    //------------------------（3）基于JSON格式存储Users对象---------------------------------------------
    /**
     * 基于JSON格式存储Users对象
     */
    @Test
    public void testJsonSaveUsers(){
        Users users = new Users();
        users.setId(2);
        users.setName("李四");
        users.setAge(26);
        this.redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(Users.class));
        this.redisTemplate.opsForValue().set("users_json",users);
    }
    /**
     * 获取JSON格式的Users
     */
    @Test
    public void testGetJsonUsers(){
        this.redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(Users.class));
        Users users = (Users)this.redisTemplate.opsForValue().get("users_json");
        System.out.println(users);//Users{id=2, name='李四', age=26}
    }


    //------------------------（4）设置redis缓存有效时间---------------------------------------------
    /**
     * 设置redis缓存有效时间
     */
    @Test
    public void setReidsTime(){
        this.redisTemplate.opsForValue().set("idcard","452226199504035414",1L, TimeUnit.MINUTES);
        //可以为天,秒,小时,微秒(一百万分之一秒). 毫秒(一千分之一秒),分钟, 纳秒(10亿分之一秒)
        //TimeUnit.SECONDS 秒单位
        //TimeUnit.MINUTES 分单位
        //TimeUnit.HOURS 小时单位
        //TimeUnit.DAYS 天单位
        //TimeUnit.MILLISECONDS 毫秒(一千分之一秒)
    }
    /**
     * 测试redis缓存有效时间
     */
    @Test
    public void testSetRedisTimeOut(){
        String idcard = (String)this.redisTemplate.opsForValue().get("idcard");
        System.out.println(idcard);
    }


    //------------------------（5）Set集合与清除redis缓存---------------------------------------------
    /**
      * 存入值
      */
    @Test
    public void setValue(){
        redisTemplate.boundSetOps("nameset").add("曹操");
        redisTemplate.boundSetOps("nameset").add("刘备");
        redisTemplate.boundSetOps("nameset").add("孙权");
    }
    /**
     * 提取值
    */
    @Test
    public void getValue(){
        Set members = redisTemplate.boundSetOps("nameset").members();
        System.out.println(members);//[刘备, 孙权, 曹操]
    }
    /**
    * 删除集合中的某一个值
    */
    @Test
    public void deleteValue(){
        redisTemplate.boundSetOps("nameset").remove("孙权");
    }
    /**
     * 删除整个集合
    */
    @Test
    public void deleteAllValue(){
        redisTemplate.delete("nameset");
    }

    //------------------------（6）List类型操作---------------------------------------------
    /**
      * 右压栈：后添加的对象排在后边
      */
    @Test
    public void testSetValue1(){
        redisTemplate.boundListOps("namelist1").rightPush("刘备");
        redisTemplate.boundListOps("namelist1").rightPush("关羽");
        redisTemplate.boundListOps("namelist1").rightPush("张飞");
    }
    /**
    * 显示右压栈集合
    */
    @Test
    public void testGetValue1(){
        List list = redisTemplate.boundListOps("namelist1").range(0, 10);
        System.out.println(list);//[刘备, 关羽, 张飞]
    }

    /**
      * 左压栈：后添加的对象排在前边
      */
    @Test
    public void testSetValue2(){
        redisTemplate.boundListOps("namelist2").leftPush("刘备");
        redisTemplate.boundListOps("namelist2").leftPush("关羽");
        redisTemplate.boundListOps("namelist2").leftPush("张飞");
    }
    /**
    * 显示左压栈集合
    */
    @Test
    public void testGetValue2(){
        List list = redisTemplate.boundListOps("namelist2").range(0, 10);
        System.out.println(list);//[张飞, 关羽, 刘备]
    }

    /**
    * 查询集合某个元素,下标从0开始
     */
    @Test
    public void testSearchByIndex(){
        String s = (String) redisTemplate.boundListOps("namelist1").index(1);
        System.out.println(s);
    }
    /**
    * 移除集合某个元素
    */
    @Test
    public void testRemoveByIndex(){
        redisTemplate.boundListOps("namelist1").remove(1, "关羽");
    }


    //------------------------（7）Hash类型操作---------------------------------------------
    /**
      * 存入值
      */
    @Test
    public void testSetValue(){
        redisTemplate.boundHashOps("namehash").put("a", "唐僧");
        redisTemplate.boundHashOps("namehash").put("b", "悟空");
        redisTemplate.boundHashOps("namehash").put("c", "八戒");
        redisTemplate.boundHashOps("namehash").put("d", "沙僧");
    }
    /**
      * 提取所有的key
      */
    @Test
    public void testGetKeys(){
        Set s = redisTemplate.boundHashOps("namehash").keys();
        System.out.println(s);//[a, b, c, d]
        List v = redisTemplate.boundHashOps("namehash").values();
        System.out.println(v);//[唐僧, 悟空, 八戒, 沙僧]
    }
    /**
     * 根据key取值
     */
    @Test
    public void testGetValueByKey(){
        String value = (String)this.redisTemplate.boundHashOps("namehash").get("a");
        System.out.println(value);//唐僧
    }
    /**
     * 根据key清除
     */
    @Test
    public void testDelByKey(){
        Long a = this.redisTemplate.boundHashOps("namehash").delete("c");
        System.out.println(a);//返回 1：操作成功，0：操作失败或者key不存在
    }



}
