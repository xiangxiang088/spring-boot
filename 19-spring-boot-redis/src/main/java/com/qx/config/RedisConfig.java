package com.qx.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Redis链接配置
 * @Configuration：表示springboot启动的时候去启动这个类
 *
 */
@Configuration
public class RedisConfig {

    /**
     * 1.创建JedisPoolConfig对象，该对象中完成一些链接池配置
     *
     * redis.timeout=15000
     * #最大连接数
     * redis.maxTotal=400
     * #最小连接数
     * redis.minIdle=100
     * #最大空闲数，数据库连接的最大空闲时间。超过空闲数量，数据库连接将被标记为不可用，然后被释放。设为0表示无限制
     * redis.maxIdle=350
     * #最大建立连接等待时间
     * redis.maxWaitMillis=10000
     * #指明是否在从池中取出连接前进行检验,如果检验失败,则从池中去除连接并尝试取出另一个,使用连接时，检测连接是否成功
     * redis.testOnBorrow=true
     * redis.usePool=true
     */
    @Bean
    @ConfigurationProperties(prefix = "spring.redis.pool")
    public JedisPoolConfig jedisPoolConfig(){
        JedisPoolConfig config = new JedisPoolConfig();
        /*//最大空闲数
        config.setMaxIdle(10);
        //最小空闲数
        config.setMinIdle(5);
        //最大连接数
        config.setMaxTotal(20);
        //最大建立连接等待时间
        config.setMaxWaitMillis(10000);*/
        System.out.println("默认配置参数："+config.getMaxIdle());
        System.out.println("默认配置参数："+config.getMinIdle());
        System.out.println("默认配置参数："+config.getMaxTotal());
        System.out.println("默认配置参数："+config.getMaxWaitMillis());
        /*默认配置参数：8
        默认配置参数：0
        默认配置参数：8
        默认配置参数：-1*/

        return config;
    }

    /**
     * 2.创建JedisConnectionFactory:配置redis信息
     *
     * redis.host=39.108.14.44
     * redis.port=6379
     * redis.pass=bigpass
     * redis.db.index=2
     *
     * redis.host=47.112.137.5
     * redis.port=6379
     * redis.pass=helloworld
     */
    @Bean
    @ConfigurationProperties(prefix = "spring.redis")
    public JedisConnectionFactory jedisConnectionFactory(JedisPoolConfig config){

        System.out.println("配置好参数后："+config.getMaxIdle());
        System.out.println("配置好参数后："+config.getMinIdle());
        System.out.println("配置好参数后："+config.getMaxTotal());
        System.out.println("配置好参数后："+config.getMaxWaitMillis());

        JedisConnectionFactory factory = new JedisConnectionFactory();
        //关联连接池的配置对象
        factory.setPoolConfig(config);
        //配置链接Redis的信息
        //主机地址
        factory.setHostName("39.108.14.44");
        //端口
        factory.setPort(6379);
        //密码
        factory.setPassword("bigpass");
        //db0、db1、db2，这里factory.setDatabase(2)设置db2
        factory.setDatabase(2);
        return factory;
    }

    /**
     * 3.创建RedisTemplate:用于执行Redis操作的方法
     */
    @Bean
    public RedisTemplate<String,Object> redisTemplate(JedisConnectionFactory factory){
        RedisTemplate<String,Object> template = new RedisTemplate<>();
        //关联
        template.setConnectionFactory(factory);
        //为key设置序列化器
        template.setKeySerializer(new StringRedisSerializer());
        //为value设置序列化
        template.setValueSerializer(new StringRedisSerializer());
        return template;
    }


}
