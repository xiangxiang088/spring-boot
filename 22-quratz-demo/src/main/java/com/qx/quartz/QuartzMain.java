package com.qx.quartz;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * 编写测试类
 */
public class QuartzMain {

    public static void main(String[] args) throws Exception{
        //1.创建对象Job：你要做什么？
        //QuartzDemo.class：这里是要执行哪个类的定时任务
        JobDetail job = JobBuilder.newJob(QuartzDemo.class).build();

        //2.创建对象Tigger,你在什么时间做？
//        方式一：(简单的 trigger 触发时间：通过 Quartz 提供一个方法来完成简单的重复 调用 cron)
//        SimpleScheduleBuilder scb = SimpleScheduleBuilder.repeatSecondlyForever();       //每秒钟执行一次
//        SimpleScheduleBuilder scb = SimpleScheduleBuilder.repeatSecondlyForever(5);      //每5秒钟执行一次
//        SimpleScheduleBuilder scb = SimpleScheduleBuilder.repeatMinutelyForTotalCount(5);//每5分钟执行一次
//        SimpleScheduleBuilder scb = SimpleScheduleBuilder.repeatHourlyForever(5);        //每5小时执行一次
//        Trigger trigger = TriggerBuilder.newTrigger().withSchedule(scb).build();
        //方式二：(Tigger:按照cron的字符串表达式方式来定时间)
        CronScheduleBuilder cron = CronScheduleBuilder.cronSchedule("0 57 21 * * ?"); //每天21:57
        Trigger trigger = TriggerBuilder.newTrigger().withSchedule(cron).build();

        //3.创建对象Scheduler，在什么时间做什么事？
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(job,trigger);
        //启动
        scheduler.start();
    }
}
