package com.qx.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import java.util.Date;

/**
 * 定义任务类
 */
public class  QuartzDemo implements Job {

    /**
     *任务触发时所执行的方法
     * @param context
     * @throws JobExecutionException if there is an exception while executing the job.
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        System.out.println("Execution......"+new Date());
    }
}
