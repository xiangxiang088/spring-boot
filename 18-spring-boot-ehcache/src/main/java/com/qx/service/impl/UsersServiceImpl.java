package com.qx.service.impl;

import com.qx.dao.UsersRepository;
import com.qx.pojo.Users;
import com.qx.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 业务层
 */
@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    @Cacheable(value = "users")
    public List<Users> findUserAll() {
        return this.usersRepository.findAll();
    }

    /**
     * @Cacheable作用：把方法返回值返回到ehcache缓存中
     * value属性：去ehcache.xml文件找自定义的chcache，如果没有给则是默认选用默认配置的ehcache.xml
     * key属性：把该方法执行后返回值保持在缓存中（再次执行该方法，如果已存在缓存直接在缓存里获取，减压数据库），默认拿属性的参数来作为key，也可以自定义key
     *
     * @param id
     * @return
     */
    @Override
    @Cacheable(value = "users") //value = "users" 去ehcache.xml文件找自定义的chcache
    public Users findUserById(Integer id) {
        return this.usersRepository.findOne(id);
    }

    @Override
    @Cacheable(value="users",key="#pageable.pageSize")//,key="#pageable.pageSize" 去掉这个你会发现，三个查询要分2次查，分别是第一和第二为1次，第三个查询第2次
    public Page<Users> findUserByPage(Pageable pageable) {
        return this.usersRepository.findAll(pageable);
    }

    /**
     * @CacheEvict(value = "users",allEntries=true)  清除缓存，每当操作数据库有改变，要清除缓存，再次获取数据库就准确
     * @param users
     */
    @Override
    @CacheEvict(value = "users",allEntries=true)
    public void saveUsers(Users users) {
        this.usersRepository.save(users);
    }
}
