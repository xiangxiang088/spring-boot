package com.qx;

import com.qx.pojo.Users;
import com.qx.service.UsersService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * 测试类
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = App.class)
public class UsersServiceTest {

    @Autowired
    private UsersService usersService;

    @Test
    public void findUserById(){
        //第一次查询
        System.out.println(this.usersService.findUserById(2));
        // 第二次查询
        System.out.println(this.usersService.findUserById(2));
    }

    @Test
    public void findUserByPage(){
        Pageable pageable = new PageRequest(0, 2);
        //第一次查询
        System.out.println(this.usersService.findUserByPage(pageable).getTotalElements());
        //第二次查询
        System.out.println(this.usersService.findUserByPage(pageable).getTotalElements());
        //第三次查询
        pageable = new PageRequest(1, 2);
        System.out.println(this.usersService.findUserByPage(pageable).getTotalElements());
    }

    @Test
    public void findUserAll(){
        //第一次查询
        System.out.println(this.usersService.findUserAll().size());

        Users users = new Users();
        users.setName("啊九");
        users.setAge(26);
        users.setAddress("人民代表大堂");
        this.usersService.saveUsers(users);

        //第二次查询
        System.out.println(this.usersService.findUserAll().size());

    }


}
