package com.bjsxt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 *SpringBoot整合Servlet方式一:通过扫描方式
 *
 */
@SpringBootApplication
@ServletComponentScan //在springBoot启动时会扫描@WebServlet,并且该类实例化
public class App 
{
    public static void main( String[] args ) {
        SpringApplication.run(App.class,args);
    }
}
