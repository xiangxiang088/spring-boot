package com.bjsxt;

import com.bjsxt.servlet.SecondServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

/**
 * SpringBoot整合Servlet方式二：通过方法实现
 */
@SpringBootApplication
public class AppSecond {

    public static void main(String[] strings){
        SpringApplication.run(AppSecond.class,strings);
    }

    @Bean
    public ServletRegistrationBean getServletRegistrationBenn(){
        ServletRegistrationBean bean = new ServletRegistrationBean(new SecondServlet());
        bean.addUrlMappings("/second");
        return bean;
    }
}
