package com.bjsxt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot访问静态资源，默认有两个默认目录，
 *
 * 一个是  classpath/static 目录 (src/mian/resource)
 * 一个是 ServletContext 根目录下( src/main/webapp )
 *
 * 这在里可能有小伙伴对 classpath 不怎么了解，这里简要的介绍下，classpath 即WEB-INF下面的classes目录 ，在springboot项目中可能就是，src/main/resource 目录。
 * ————————————————
 * 版权声明：本文为CSDN博主「临窗，听雨声」的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。
 * 原文链接：https://blog.csdn.net/yali_aini/article/details/83213695
 *
 */
@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {
        SpringApplication.run(App.class,args);
        System.out.println( "Hello World!" );
    }
}
