package com.qx.pojo;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.util.Date;

/**
 * 笔记总结：
 * @NotBlank 判断字符串是否为null或者为空字符串（自动去掉收尾空格）
 * @NotEmpty 判断字符串是否为null或者为空字符串（不会自动去掉空格）
 * @Length(min = 6,max = 18)  长度需要在6和18之间
 * @Min(2) 最小不能小于2
 * @Max(6) 最大不能超过6
 * @Eamil 邮箱验证:不是一个合法的电子邮件地址
 */

public class User {

    //@NotBlank(message = "用户名不能为空") //非空检验
    @NotEmpty
    private String userName;
    @NotBlank(message = "用户密码不能为空") //用户密码非空检验
    @Length(min = 6,max = 18)
    private String password;
    @Min(6)
    private Integer age;
    @Email
    private String email;

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", age=" + age +
                '}';
    }
}
