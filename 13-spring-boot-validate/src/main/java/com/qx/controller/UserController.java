package com.qx.controller;

import com.qx.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
public class UserController {

    /**
     * 1.页面跳转:thymeleaf里面的方法
     * 例如：http://127.0.0.1:8080/users/input
     * @return
     */
    @RequestMapping("/{page}")
    public String showPage(@PathVariable String page){ //@PathVariable是spring3.0的一个新功能：接收请求路径中占位符的值
        return page;
    }

    /**
     * 2.打开添加用户界面
     * Whitelabel Error Page
     * This application has no explicit mapping for /error, so you are seeing this as a fallback.
     *
     * Mon Feb 03 22:12:13 CST 2020
     * There was an unexpected error (type=Internal Server Error, status=500).
     * Error during execution of processor 'org.thymeleaf.spring5.processor.SpringErrorsTagProcessor' (template: "addUser" - line 9, col 73)
     *
     * 解决异常方式1：【推荐】
     * 可以在跳转页面的方法注入一个User对象。
     * 注意：由于springmvc会将该对象的Model中传递。key的名称会使用该对象的驼峰式命名规则来作为key.
     * 参数的变量名需要与对象的名称相同，将首字母小写。
     *
     * 解决方式2：
     * @ModelAttribute("us"),如果自己想按自定义命名，就用此方法.
     */
    @RequestMapping("/addUser")
    public String addUser(@ModelAttribute("us") User user){
        return "addUser";
    }

    /**
     * 3.保存用户信息
     * @Valid 开始对User用户对象检验
     * BindingResult 开始检验结果返回
     * if(result.hasErrors()) 如果返回ture,表面校验不通过，反之false
     */
    @RequestMapping("/save")
    public String saveUser(@ModelAttribute("us") @Valid User user,BindingResult result){
        System.out.println("user="+user);
        if(result.hasErrors()){
            return "addUser";
        }
        return "success";
    }





}
