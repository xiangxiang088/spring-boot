package com.qx.quartz;

import com.qx.service.UsersService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
/**
 * 创建Job定时任务方法类
 */
public class QuartzDemo implements Job {

    @Autowired
    private UsersService usersService;

    /**
     * 定时任务所要执行的方法
     * @param context
     * @throws JobExecutionException if there is an exception while executing the job.
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        System.out.println("SpirngBoot定时任务整合Quartz框架"+new Date());
        this.usersService.OrderCancel();
    }
}
