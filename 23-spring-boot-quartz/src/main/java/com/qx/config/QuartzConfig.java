package com.qx.config;

import com.qx.quartz.QuartzDemo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import java.util.Date;

/**
 * Quartz配置类
 */
@Configuration
public class QuartzConfig {

    /**
     * （1）创建Job对象，你要做什么？
     */
    @Bean
    public JobDetailFactoryBean jobDetailFactoryBean(){
        JobDetailFactoryBean factory = new JobDetailFactoryBean();
        //关联我们的任务类
        factory.setJobClass(QuartzDemo.class);
        return factory;
    }

//    -------------------------------方式一：（简单Tigger）----------------------------
//    /**
//     * （2）创建Tigger对象，你什么时候要去执行？
//     */
//    @Bean
//    public SimpleTriggerFactoryBean simpleTriggerFactoryBean(JobDetailFactoryBean jobDetailFactoryBean){
//        SimpleTriggerFactoryBean factoryBean = new SimpleTriggerFactoryBean();
//        //关联我们的Job对象
//        factoryBean.setJobDetail(jobDetailFactoryBean.getObject());
//        //表示执行的毫秒数1000=1秒 2000=2秒
//        factoryBean.setRepeatInterval(2000);
//        //执行3次
//        //factoryBean.setRepeatCount(3); 如果注释掉这里，系统将会每2秒执行无数次
//        return factoryBean;
//    }
//    /**
//     * （3）创建Scheduler对象，什么时候去执行什么事情？
//     */
//    @Bean
//    public SchedulerFactoryBean schedulerFactoryBean(SimpleTriggerFactoryBean simpleTriggerFactoryBean){
//        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
//        //关联Tigger对象
//        schedulerFactoryBean.setTriggers(simpleTriggerFactoryBean.getObject());
//        return schedulerFactoryBean;
//    }

    //    ---------------------------------方式二(cron表达式)--------------------------------
    /**
     * （2）创建Tigger对象，你什么时候要去执行？
     */
    @Bean
    public CronTriggerFactoryBean cronTriggerFactoryBean(JobDetailFactoryBean jobDetailFactoryBean){
        CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();
        //关联我们的Job对象
        factoryBean.setJobDetail(jobDetailFactoryBean.getObject());
        //表示执行的毫秒数1000=1秒 2000=2秒
        factoryBean.setCronExpression("0/2 * * * * ?");
        return factoryBean;
    }
    /**
     * （3）创建Scheduler对象，什么时候去执行什么事情？
     */
    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(CronTriggerFactoryBean cronTriggerFactoryBean,MyAdaptableJobFactory myAdaptableJobFactory){
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        //关联Tigger对象
        schedulerFactoryBean.setTriggers(cronTriggerFactoryBean.getObject());
        schedulerFactoryBean.setJobFactory(myAdaptableJobFactory);
        return schedulerFactoryBean;
    }

}
